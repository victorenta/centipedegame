﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitObjectTypes
{
    None = 0
    , Mushroom
    , Centipede
    , Bullet
    , Player
}

public enum DirectionTypes
{
    North
    , East
    , South
    , West
}

public enum ConfigTypes
{
    BoardWidth
    , PlayerLifePoint
    , PlayerSpeed
    , BulletSpeed
    , CentipedeLength
    , CentipedeSpeed
    , MushroomLifePoint
}