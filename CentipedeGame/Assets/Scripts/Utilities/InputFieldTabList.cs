﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputFieldTabList : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] List<TMP_InputField> _Input_List;
    #endregion

    #region Private Properties
    private int _inputFieldIndex = 0;
    #endregion

    #region Methods
    private void Start()
    {
        if (_Input_List == null)
        {
            return;
        }
        EventSystem.current.SetSelectedGameObject(_Input_List[_inputFieldIndex].gameObject);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (_Input_List == null)
            {
                return;
            }

            _inputFieldIndex++;
            if (_inputFieldIndex >= _Input_List.Count)
            {
                _inputFieldIndex = 0;
            }

            EventSystem.current.SetSelectedGameObject(_Input_List[_inputFieldIndex].gameObject);
        }
    }
    #endregion
}