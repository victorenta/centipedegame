﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private Grid _Grid;
    [SerializeField] private Tilemap _TileMap;
    [SerializeField] private Sprite _TileImage;
    #endregion

    #region Public Properties
    public static BoardManager Instance { get { return _instance; } }

    public int Width { get { return _width; } }
    public int Height { get { return _height; } }
    #endregion

    #region Private Properties
    private static BoardManager _instance = null;

    private Tile _tile;
    private int _width = 32;
    private int _height = 32;
    private float _cellHalfSizeX;
    private float _cellHalfSizeY;

    private List<TileData> _tileDataList = new List<TileData>();

    private const float _maxHeightPercentForPlayer = 0.15f;
    #endregion

    #region Methods
    private void InitSingleton()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
        //DontDestroyOnLoad(this.gameObject);
    }

    private void Awake()
    {
        InitSingleton();
    }

    public void Init()
    {
        SetGrid();
        SetCamera();
    }

    private void SetGrid()
    {
        // clear all tile
        _TileMap.ClearAllTiles();

        // clear tile data list
        _tileDataList.Clear();

        // init tile
        _tile = ScriptableObject.CreateInstance<Tile>();
        _tile.sprite = _TileImage;

        // get cell size
        _cellHalfSizeX = _Grid.cellSize.x / 2;
        _cellHalfSizeY = _Grid.cellSize.y / 2;

        for (int y = 0; y < _height; y++)
        {
            for (int x = 0; x < _width; x++)
            {
                _TileMap.SetTile(new Vector3Int(x, y, 0), _tile);
                _tileDataList.Add(new TileData(new Vector2(x + _cellHalfSizeX, y + _cellHalfSizeY)));
            }
        }
    }

    private void SetCamera()
    {
        float screenRatio = (float)Screen.width / (float)Screen.height;
        float targetRatio = _TileMap.localBounds.size.x / _TileMap.localBounds.size.y;

        if (screenRatio >= targetRatio)
        {
            Camera.main.orthographicSize = _TileMap.localBounds.size.y / 2;
        }
        else
        {
            float differenceInSize = targetRatio / screenRatio;
            Camera.main.orthographicSize = _TileMap.localBounds.size.y / 2 * differenceInSize;
        }

        Camera.main.transform.localPosition = new Vector3((float)_width / 2, (float)_height / 2, -10);
    }

    public void SetBoardWidth(int width)
    {
        _width = Mathf.Abs(width);
    }

    public void SetBoardHeight(int height)
    {
        _height = Mathf.Abs(height);
    }

    public List<TileData> GetTopLeftTileDataList(int count, int yDiff = 0)
    {
        List<TileData> result = new List<TileData>();
        TileData tile = null;
        for(int i = 0; i < count; i++)
        {
            tile = _tileDataList.Find(x => x.TilePosition == new Vector2(i + _cellHalfSizeX, _height - _cellHalfSizeY));
            if (tile != null)
            {
                result.Add(tile);
            }

            for (int j = 1; j < yDiff; j++)
            {
                tile = _tileDataList.Find(x => x.TilePosition == new Vector2(i + _cellHalfSizeX, (_height - j) - _cellHalfSizeY));
                if (tile != null)
                {
                    result.Add(tile);
                }
            }
        }

        return result;
    }

    public List<TileData> GetButtomLeftTileDataList(int count, int yDiff = 0)
    {
        List<TileData> result = new List<TileData>();
        TileData tile = null;
        for (int i = 0; i < count; i++)
        {
            tile = _tileDataList.Find(x => x.TilePosition == new Vector2(i + _cellHalfSizeX, _cellHalfSizeY));
            if (tile != null)
            {
                result.Add(tile);
            }

            for (int j = 1; j < yDiff; j++)
            {
                tile = _tileDataList.Find(x => x.TilePosition == new Vector2(i + _cellHalfSizeX, j + _cellHalfSizeY));
                if (tile != null)
                {
                    result.Add(tile);
                }
            }
        }

        return result;
    }

    public List<TileData> GetSideLeftTileDataList(int count, int xDiff = 0)
    {
        List<TileData> result = new List<TileData>();
        TileData tile = null;
        for (int i = 0; i < count; i++)
        {
            tile = _tileDataList.Find(x => x.TilePosition == new Vector2(_cellHalfSizeX, i + _cellHalfSizeY));
            if (tile != null)
            {
                result.Add(tile);
            }

            for (int j = 1; j < xDiff; j++)
            {
                tile = _tileDataList.Find(x => x.TilePosition == new Vector2(j + _cellHalfSizeX, i + _cellHalfSizeY));
                if (tile != null)
                {
                    result.Add(tile);
                }
            }
        }

        return result;
    }

    public List<TileData> GetSideRightTileDataList(int count, int xDiff = 0)
    {
        List<TileData> result = new List<TileData>();
        TileData tile = null;
        for (int i = 0; i < count; i++)
        {
            tile = _tileDataList.Find(x => x.TilePosition == new Vector2(_width - _cellHalfSizeX, i + _cellHalfSizeY));
            if (tile != null)
            {
                result.Add(tile);
            }

            for (int j = 1; j < xDiff; j++)
            {
                tile = _tileDataList.Find(x => x.TilePosition == new Vector2((_width - j) - _cellHalfSizeX, i + _cellHalfSizeY));
                if (tile != null)
                {
                    result.Add(tile);
                }
            }
        }

        return result;
    }

    public List<TileData> GetCrossTileDataList(TileData centerTile)
    {
        List<TileData> result = new List<TileData>();
        TileData foundData = null;
        foundData = _tileDataList.Find(tile => tile.TilePosition == new Vector2(centerTile.TilePosition.x - 1, centerTile.TilePosition.y + 1));
        if (foundData != null)
        {
            result.Add(foundData);
        }

        foundData = _tileDataList.Find(tile => tile.TilePosition == new Vector2(centerTile.TilePosition.x + 1, centerTile.TilePosition.y + 1));
        if (foundData != null)
        {
            result.Add(foundData);
        }

        foundData = _tileDataList.Find(tile => tile.TilePosition == new Vector2(centerTile.TilePosition.x - 1, centerTile.TilePosition.y - 1));
        if (foundData != null)
        {
            result.Add(foundData);
        }

        foundData = _tileDataList.Find(tile => tile.TilePosition == new Vector2(centerTile.TilePosition.x + 1, centerTile.TilePosition.y - 1));
        if (foundData != null)
        {
            result.Add(foundData);
        }

        return result;
    }

    public bool IsTileValid(Vector2 tilePosition)
    {
        return _tileDataList.Find(x => x.TilePosition == tilePosition) != null;
    }

    public bool TryGetTileData(Vector2 tilePosition, out TileData result)
    {
        result = _tileDataList.Find(x => x.TilePosition == tilePosition);
        if(result == null)
        {
            return false;
        }

        return true;
    }

    public Dictionary<DirectionTypes, TileData> GetAdjacentTileData(Vector2 centerPosition)
    {
        float xPos = centerPosition.x;
        float yPos = centerPosition.y;
        Dictionary<DirectionTypes, TileData> result = new Dictionary<DirectionTypes, TileData>();
        int centerTileIndex = _tileDataList.FindIndex(x => x.TilePosition == centerPosition);

        result.Add(DirectionTypes.North, null);
        result.Add(DirectionTypes.East, null);
        result.Add(DirectionTypes.South, null);
        result.Add(DirectionTypes.West, null);
        if (centerTileIndex >= 0)
        {
            result[DirectionTypes.North] = _tileDataList.Find(x => x.TilePosition == new Vector2(xPos, yPos + 1));
            result[DirectionTypes.East] = _tileDataList.Find(x => x.TilePosition == new Vector2(xPos + 1, yPos));
            result[DirectionTypes.South] = _tileDataList.Find(x => x.TilePosition == new Vector2(xPos, yPos - 1));
            result[DirectionTypes.West] = _tileDataList.Find(x => x.TilePosition == new Vector2(xPos - 1, yPos));
        }

        return result;
    }

    public List<TileData> GetRandomTileDataList(List<TileData> restrictedTilePosition, int countPerRow)
    {
        Random.InitState(DateTime.UtcNow.Millisecond);
        List<TileData> result = new List<TileData>();
        List<TileData> rowList = new List<TileData>();
        int indexCount = 0;
        for (int i = 0; i < _tileDataList.Count; i++)
        {
            indexCount++;
            if (restrictedTilePosition.Find(x => x.TilePosition == _tileDataList[i].TilePosition) == null)
            {
                rowList.Add(_tileDataList[i]);
            }

            if(indexCount > 0 && indexCount % _width == 0)
            {
                List<TileData> resultRow = GetRandomTileDataListFromList(rowList, countPerRow);
                foreach (TileData tile in resultRow)
                {
                    restrictedTilePosition.AddRange(GetCrossTileDataList(tile));
                }

                result.AddRange(resultRow);
                rowList.Clear();
                indexCount = 0;
            }
        }

        return result;
    }

    private List<TileData> GetRandomTileDataListFromList(List<TileData> inputList, int count)
    {
        List<TileData> inputTileList = new List<TileData>(inputList);
        List<int> indexList = new List<int>();
        for(int i = 0; i < inputTileList.Count; i++)
        {
            indexList.Add(i);
        }

        List<TileData> result = new List<TileData>();
        for(int i = 0; i < count; i++)
        {
            if(inputTileList.Count == 0)
            {
                break;
            }

            int index = Random.Range(0, (indexList.Count - 1));
            result.Add(inputTileList[indexList[index]]);
            indexList.RemoveAt(index);
        }

        return result;
    }

    public List<TileData> GetValidTileForPlayerObject()
    {
        List<TileData> result = new List<TileData>();
        int maxHeight = Mathf.RoundToInt(_height * _maxHeightPercentForPlayer);
        return GetButtomLeftTileDataList(_width, maxHeight);
    }

    public Vector2 GetStartPlayerPosition()
    {
        int centerWidth = (int)(_width / 2);
        return new Vector2(centerWidth + _cellHalfSizeX, _cellHalfSizeY);
    }

    public void RequestMoveIntoTile(UnitObject unitObject)
    {
        UnsubscribeFromTile(unitObject, unitObject.PreviousPosition);
        SubscribeIntoTile(unitObject, unitObject.CurrentPosition);
    }

    public void RequestMoveOutFromTile(UnitObject unitObject)
    {
        UnsubscribeFromTile(unitObject, unitObject.CurrentPosition);
    }

    private void SubscribeIntoTile(UnitObject unitObject, Vector2 tilePosition)
    {
        if(TryGetTileData(tilePosition, out TileData tile))
        {
            unitObject.SubscribeTile(tile);
            tile.SubscribeUnit(unitObject.Type);
        }
    }

    private void UnsubscribeFromTile(UnitObject unitObject, Vector2 tilePosition)
    {
        if (TryGetTileData(tilePosition, out TileData tile))
        {
            unitObject.UnsubscribeTile(tile);
            tile.UnsubscribeUnit(unitObject.Type);
        }
    }
    #endregion
}