﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SettingPanelManager : MonoBehaviour
{
    #region Inspector Properties
    [SerializeField] private TMP_InputField _Input_BoardWidth;
    [SerializeField] private TMP_InputField _Input_PlayerLifePoint;
    [SerializeField] private TMP_InputField _Input_PlayerSpeed;
    [SerializeField] private TMP_InputField _Input_BulletSpeed;
    [SerializeField] private TMP_InputField _Input_CentipedeLength;
    [SerializeField] private TMP_InputField _Input_CentipedeSpeed;
    [SerializeField] private TMP_InputField _Input_MushroomLifePoint;
    [SerializeField] private Button _Button_Accept;
    #endregion

    #region Private Properties
    private int _minBoardWidth = 15;
    private int _minPlayerLifePoint = 1;
    private int _minPlayerSpeed = 1;
    private int _minBulletSpeed = 1;
    private int _minCentipedeLength = 1;
    private int _minCentipedeSpeed = 1;
    private int _minMushroomHP = 1;
    #endregion

    #region Methods
    private void Start()
    {
        InitUIEvents();
    }

    private void InitUIEvents()
    {
        _Button_Accept.onClick.RemoveAllListeners();
        _Button_Accept.onClick.AddListener(OnAccept);
    }

    private int GetValueFromInputText(TMP_InputField input, int minimumValue)
    {
        if(string.IsNullOrEmpty(input.text))
        {
            input.text = "0";
        }

        int maximumValue = 1;
        int.TryParse(input.text, out maximumValue);
        return GetValueFromInputText(input, minimumValue, maximumValue);
    }

    private int GetValueFromInputText(TMP_InputField input, int minimumValue, int maximumValue)
    {
        if (string.IsNullOrEmpty(input.text))
        {
            input.text = "0";
        }

        int result = 1;
        if(int.TryParse(input.text, out result))
        {
            result = Mathf.Clamp(result, minimumValue, maximumValue);
        }
        return result;
    }

    private void OnAccept()
    {
        Dictionary<ConfigTypes, int> configData = new Dictionary<ConfigTypes, int>();
        int boardWidth = GetValueFromInputText(_Input_BoardWidth, _minBoardWidth);
        configData.Add(ConfigTypes.BoardWidth, boardWidth);
        configData.Add(ConfigTypes.PlayerLifePoint, GetValueFromInputText(_Input_PlayerLifePoint, _minPlayerLifePoint));
        configData.Add(ConfigTypes.PlayerSpeed, GetValueFromInputText(_Input_PlayerSpeed, _minPlayerSpeed));
        configData.Add(ConfigTypes.BulletSpeed, GetValueFromInputText(_Input_BulletSpeed, _minBulletSpeed));
        configData.Add(ConfigTypes.CentipedeLength, GetValueFromInputText(_Input_CentipedeLength, _minCentipedeLength, boardWidth));
        configData.Add(ConfigTypes.CentipedeSpeed, GetValueFromInputText(_Input_CentipedeSpeed, _minCentipedeSpeed));
        configData.Add(ConfigTypes.MushroomLifePoint, GetValueFromInputText(_Input_MushroomLifePoint, _minMushroomHP));

        UIManager.Instance.RequestStartGame(configData);
    }
    #endregion
}