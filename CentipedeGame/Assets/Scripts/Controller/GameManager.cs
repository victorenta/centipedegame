﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    #region Inpector Properties
    [SerializeField] private GameObject _Prefab_Centipede;
    [SerializeField] private Transform _Group_Centipede;
    [SerializeField] private GameObject _Prefab_Mushroom;
    [SerializeField] private Transform _Group_Mushroom;
    [SerializeField] private GameObject _Prefab_Player;
    [SerializeField] private GameObject _Prefab_Bullet;
    [SerializeField] private Transform _Group_Bullet;
    #endregion

    #region Public Properties
    public static GameManager Instance { get { return _instance; } }
    #endregion

    #region Private Properties
    private static GameManager _instance = null;

    private int _sumScore = 0;
    private int _centipedeScore = 100;

    private int _centipedeSpeed = 10;
    private int _centipedeLength = 35;

    private int _mushroomMaxHP = 3;

    private int _playerMaxHP = 3;
    private int _playerCurrentHP = 0;

    private int _playerSpeed = 15;
    private int _bulletSpeed = 100;

    private int _boardWidth = 32;
    private int _boardHeight = 32;

    private bool _isGameEnd = false;

    private List<CentipedeObject> _centipedeObjectList = new List<CentipedeObject>();
    private List<MushroomObject> _mushroomObjectList = new List<MushroomObject>();
    private PlayerObject _playerObject;
    private List<BulletObject> _bulletObjectList = new List<BulletObject>();

    private List<UnitObject> _destroyedCentipedeList = new List<UnitObject>();
    #endregion

    #region Methods
    private void InitSingleton()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
        //DontDestroyOnLoad(this.gameObject);
    }

    private void Awake()
    {
        InitSingleton();
    }

    public void RequestStartGame(Dictionary<ConfigTypes, int> configData)
    {
        configData.TryGetValue(ConfigTypes.BoardWidth, out _boardWidth);
        configData.TryGetValue(ConfigTypes.BoardWidth, out _boardHeight);
        configData.TryGetValue(ConfigTypes.PlayerLifePoint, out _playerMaxHP);
        configData.TryGetValue(ConfigTypes.PlayerSpeed, out _playerSpeed);
        configData.TryGetValue(ConfigTypes.BulletSpeed, out _bulletSpeed);
        configData.TryGetValue(ConfigTypes.CentipedeLength, out _centipedeLength);
        configData.TryGetValue(ConfigTypes.CentipedeSpeed, out _centipedeSpeed);
        configData.TryGetValue(ConfigTypes.MushroomLifePoint, out _mushroomMaxHP);

        // setup everything !!!
        SetupData();
    }

    private void SetupData()
    {
        InitBoard();
        InitCentipede();
        InitMushroom();
        InitPlayer();
        UpdateUI();
    }

    private void ReSetupData()
    {
        ReInitCentipede();
        ReInitMushroom();
        ReInitPlayer();
        UpdateUI();
    }

    private void UpdateUI()
    {
        UIManager.Instance.RequestSetScore(_sumScore);
        UIManager.Instance.RequestSetLifePoint(_playerMaxHP - _playerCurrentHP);
    }

    private float GetTimeIntervalSpeed(float speed)
    {
        return 1 / speed;
    }

    private void InitBoard()
    {
        BoardManager.Instance.SetBoardWidth(_boardWidth);
        BoardManager.Instance.SetBoardHeight(_boardHeight);
        BoardManager.Instance.Init();
    }

    private void InitCentipede()
    {
        _centipedeLength = Mathf.Clamp(_centipedeLength, 1, BoardManager.Instance.Width);
        List<TileData> topLeftTileList = BoardManager.Instance.GetTopLeftTileDataList(_centipedeLength);
        int reverseIndex = _centipedeLength - 1;
        for (int i = 0; i < _centipedeLength; i++)
        {
            Vector3 startPosition = new Vector3(topLeftTileList[reverseIndex].TilePosition.x, topLeftTileList[reverseIndex].TilePosition.y, 0);
            GameObject centipede = Instantiate(_Prefab_Centipede, startPosition, Quaternion.identity, _Group_Centipede);
            _centipedeObjectList.Add(centipede.GetComponent<CentipedeObject>());

            reverseIndex--;
        }

        CentipedeObject parent = null;
        for (int i = 0; i < _centipedeObjectList.Count; i++)
        {
            if(i > 0)
            {
                parent = _centipedeObjectList[i - 1];
            }

            _centipedeObjectList[i].Init(parent);
        }

        RequestStartMoveCentipede(_centipedeObjectList[0]);
    }

    private void ReInitCentipede()
    {
        List<CentipedeObject> remainedCentipede = new List<CentipedeObject>();
        for(int i = 0; i < _centipedeObjectList.Count; i++)
        {
            if(_centipedeObjectList[i].gameObject.activeSelf)
            {
                remainedCentipede.Add(_centipedeObjectList[i]);
            }
        }

        CentipedeObject parent = null;
        List<TileData> topLeftTileList = BoardManager.Instance.GetTopLeftTileDataList(remainedCentipede.Count);
        int reverseIndex = remainedCentipede.Count - 1;
        for (int i = 0; i < remainedCentipede.Count; i++)
        {
            Vector2 startPosition = new Vector3(topLeftTileList[reverseIndex].TilePosition.x, topLeftTileList[reverseIndex].TilePosition.y);
            if (i > 0)
            {
                parent = remainedCentipede[i - 1];
            }
            remainedCentipede[i].ReInit(startPosition, parent);

            reverseIndex--;
        }
        RequestStartMoveCentipede(remainedCentipede[0]);
    }

    public void RequestStartMoveCentipede(CentipedeObject centipede)
    {
        centipede.StartMove(GetTimeIntervalSpeed(_centipedeSpeed));
    }

    private void InitMushroom()
    {
        List<TileData> restrictedTileList = new List<TileData>();
        restrictedTileList.AddRange(BoardManager.Instance.GetTopLeftTileDataList(BoardManager.Instance.Width));
        restrictedTileList.AddRange(BoardManager.Instance.GetButtomLeftTileDataList(BoardManager.Instance.Width));
        restrictedTileList.AddRange(BoardManager.Instance.GetSideLeftTileDataList(BoardManager.Instance.Height));
        restrictedTileList.AddRange(BoardManager.Instance.GetSideRightTileDataList(BoardManager.Instance.Height));

        List<TileData> randomResultTile = BoardManager.Instance.GetRandomTileDataList(restrictedTileList, 2);
        for(int i = 0; i < randomResultTile.Count; i++)
        {
            if(restrictedTileList.Find(x => x.TilePosition == randomResultTile[i].TilePosition) != null)
            {
                continue;
            }

            GameObject mushroom = Instantiate(_Prefab_Mushroom, new Vector3(randomResultTile[i].TilePosition.x, randomResultTile[i].TilePosition.y, 0), Quaternion.identity, _Group_Mushroom);
            MushroomObject mushroomObject = mushroom.GetComponent<MushroomObject>();
            mushroomObject.Init(_mushroomMaxHP);
            _mushroomObjectList.Add(mushroomObject);
        }
    }

    private void ReInitMushroom()
    {
        for (int i = 0; i < _mushroomObjectList.Count; i++)
        {
            _mushroomObjectList[i].ReInit(_mushroomMaxHP);
        }
    }

    private void InitPlayer()
    {
        Vector2 startPosition = BoardManager.Instance.GetStartPlayerPosition();
        _playerObject = Instantiate(_Prefab_Player, new Vector3(startPosition.x, startPosition.y, 0), Quaternion.identity).GetComponent<PlayerObject>();
        _playerObject.Init(startPosition, GetTimeIntervalSpeed(_playerSpeed));
    }

    private void ReInitPlayer()
    {
        Vector2 startPosition = BoardManager.Instance.GetStartPlayerPosition();
        _playerObject.ReInit(startPosition, GetTimeIntervalSpeed(_playerSpeed));

        //foreach (BulletObject item in _bulletObjectList)
        //{
        //    if (item.gameObject.activeSelf == false)
        //    {
        //        item.DeInit();
        //        return;
        //    }
        //}
    }

    public void RequestShootBullet()
    {
        float speed = GetTimeIntervalSpeed(_bulletSpeed);
        if (_bulletObjectList.Count > 0)
        {
            foreach(BulletObject item in _bulletObjectList)
            {
                if (item.gameObject.activeSelf == false)
                {
                    item.Init(speed, _playerObject.CurrentPosition);
                    return;
                }
            } 
        }

        GameObject bullet = null;
        bullet = Instantiate(_Prefab_Bullet, _playerObject.CurrentPosition, Quaternion.identity, _Group_Bullet);
        BulletObject bulletObject = bullet.GetComponent<BulletObject>();
        bulletObject.Init(speed, _playerObject.CurrentPosition);
        _bulletObjectList.Add(bulletObject);
    }

    public void RequestOnUnitObjectDestroyed(UnitObject unitObject)
    {
        switch (unitObject.Type)
        {
            case UnitObjectTypes.Centipede:
            {
                _sumScore += _centipedeScore;
                _destroyedCentipedeList.Add(unitObject);
                if(_destroyedCentipedeList.Count >= _centipedeObjectList.Count)
                {
                    // end game
                    OnGameEnd();
                }
            }
            break;

            case UnitObjectTypes.Player:
            {
                _playerCurrentHP++;
                if(_playerCurrentHP >= _playerMaxHP)
                {
                    // end game
                    OnGameEnd();
                }
                else
                {
                    ReSetupData();
                }
            }
            break;
        }
        UpdateUI();
    }

    private void OnGameEnd()
    {
        Debug.Log("END GAME");

        Time.timeScale = 0;
        _isGameEnd = true;

        UIManager.Instance.RequestShowEndGame();
        UIManager.Instance.OnKeyPressDown += RestartGame;
    }

    private void RestartGame(KeyCode inputKey)
    {
        SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
        Time.timeScale = 1;
    }
    #endregion
}