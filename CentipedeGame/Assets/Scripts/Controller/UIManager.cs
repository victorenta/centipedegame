﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

public class UIManager : MonoBehaviour
{
    #region Inpector Properties
    [SerializeField] private GameObject _Panel_GameOver;
    [SerializeField] private TMP_Text _Text_Score;
    [SerializeField] private TMP_Text _Text_LifePoint;
    [SerializeField] private GameObject _Panel_Config;
    #endregion

    #region Public Properties
    public static UIManager Instance { get { return _instance; } }
    #endregion

    #region Private Properties
    private static UIManager _instance = null;
    #endregion

    #region Events
    public event UnityAction<KeyCode> OnKeyPress;
    public event UnityAction<KeyCode> OnKeyPressDown;
    public event UnityAction<KeyCode> OnKeyPressUp;
    #endregion

    #region Methods
    private void InitSingleton()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
        //DontDestroyOnLoad(this.gameObject);
    }

    private void Awake()
    {
        InitSingleton();
    }

    private void Update()
    {
        GetKeyDown();
        GetKeyUp();
    }

    private void GetKeyDown()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            OnKeyPressDown?.Invoke(KeyCode.UpArrow);
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            OnKeyPressDown?.Invoke(KeyCode.RightArrow);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            OnKeyPressDown?.Invoke(KeyCode.DownArrow);
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            OnKeyPressDown?.Invoke(KeyCode.LeftArrow);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnKeyPressDown?.Invoke(KeyCode.Space);
        }
    }

    private void GetKeyUp()
    {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            OnKeyPressUp?.Invoke(KeyCode.UpArrow);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            OnKeyPressUp?.Invoke(KeyCode.RightArrow);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            OnKeyPressUp?.Invoke(KeyCode.DownArrow);
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            OnKeyPressUp?.Invoke(KeyCode.LeftArrow);
        }
    }

    public void RequestStartGame(Dictionary<ConfigTypes, int> configData)
    {
        _Panel_Config.SetActive(false);
        GameManager.Instance.RequestStartGame(configData);
    }

    public void RequestSetScore(int score)
    {
        _Text_Score.text = string.Format("Score: {0}", score);
    }

    public void RequestSetLifePoint(int life)
    {
        _Text_LifePoint.text = string.Format("Life: {0}", life);
    }

    public void RequestShowEndGame()
    {
        _Panel_GameOver.SetActive(true);
    }
    #endregion
}
