﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletObject : UnitObject
{
    #region Public Properties
    public override UnitObjectTypes Type => UnitObjectTypes.Bullet;
    #endregion

    #region Private Properties
    #endregion

    #region Methods
    public void Init(float speed, Vector2 startPosition)
    {
        _timeSpeedInterval = speed;
        UpdatePosition(startPosition);

        gameObject.SetActive(true);
        StartCoroutine(IEDoMove());
    }

    private IEnumerator IEDoMove()
    {
        while(true)
        {
            DoMove();
            yield return new WaitForSeconds(_timeSpeedInterval);
        }
    }

    private void DoMove()
    {
        Vector2 nextPosition = new Vector2(transform.position.x, transform.position.y + 1);
        if (IsTilePositionValid(nextPosition))
        {
            UpdatePosition(nextPosition);
        }
        else
        {
            //detroy
            DeInit();
        }
    }

    protected override void CalculateRelation(UnitObjectTypes interactorType)
    {
        switch (interactorType)
        {
            case UnitObjectTypes.Centipede:
            case UnitObjectTypes.Mushroom:
            case UnitObjectTypes.Bullet:
            {
                DeInit();
            }
            break;
        }
        //Debug.LogFormat("{0} hit {1}", Type.ToString(), interactorType.ToString());
    }
    #endregion
}