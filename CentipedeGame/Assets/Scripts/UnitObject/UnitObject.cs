﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class UnitObject : MonoBehaviour
{
    #region Public Properties
    public abstract UnitObjectTypes Type { get; }
    public Vector2 PreviousPosition { get; protected set; }
    public Vector2 CurrentPosition { get; protected set; }
    #endregion

    #region Protected Properties
    protected float _timeSpeedInterval = 0.0f;
    #endregion

    #region Methods
    protected virtual void CalculateRelation(UnitObjectTypes interactorType)
    {
        Debug.LogFormat("{0} hit {1}", Type.ToString(), interactorType.ToString());
    }

    protected virtual void UpdatePosition(Vector2 updatedPosition)
    {
        DoUpdatePosition(updatedPosition, CurrentPosition);
    }

    protected void DoUpdatePosition(Vector2 updatedPosition, Vector2 previousPosition)
    {
        PreviousPosition = previousPosition;
        transform.position = new Vector3(updatedPosition.x, updatedPosition.y, 0);
        CurrentPosition = new Vector2(transform.position.x, transform.position.y);

        BoardManager.Instance.RequestMoveIntoTile(this);
    }

    protected bool IsTileHasMushroom(TileData tile)
    {
        bool result = true;
        if (tile == null || tile.IsHasUnitType(UnitObjectTypes.Mushroom))
        {
            return false;
        }

        return result;
    }

    protected bool IsTilePositionValid(Vector2 tilePosition)
    {
        return BoardManager.Instance.IsTileValid(tilePosition);
    }

    public void SubscribeTile(TileData tile)
    {
        tile.OnBeInteracted += CalculateRelation;
    }

    public void UnsubscribeTile(TileData tile)
    {
        tile.OnBeInteracted -= CalculateRelation;
    }

    public virtual void DeInit()
    {
        StopAllCoroutines();
        BoardManager.Instance.RequestMoveOutFromTile(this);
        gameObject.SetActive(false);
    }
    #endregion
}