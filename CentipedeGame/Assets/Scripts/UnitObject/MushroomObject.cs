﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MushroomObject : UnitObject
{
    #region Public Properties
    public override UnitObjectTypes Type => UnitObjectTypes.Mushroom;
    #endregion

    #region Private Properties
    private SpriteRenderer _renderer;

    private float _maxPoint = 3;
    private float _hitPoint = 0;
    #endregion

    #region Methods
    public void Init(int maxHP)
    {
        gameObject.SetActive(true);
        _maxPoint = maxHP;
        _hitPoint = 0;
        UpdatePosition(new Vector2(transform.position.x, transform.position.y));
        _renderer = gameObject.GetComponent<SpriteRenderer>();
        UpdateVisual();
    }

    public void ReInit(int maxHP)
    {
        gameObject.SetActive(true);
        _maxPoint = maxHP;
        _hitPoint = 0;
        UpdatePosition(new Vector2(transform.position.x, transform.position.y));
        _renderer = gameObject.GetComponent<SpriteRenderer>();
        UpdateVisual();
    }

    protected override void CalculateRelation(UnitObjectTypes interactorType)
    {
        switch (interactorType)
        {
            case UnitObjectTypes.Bullet:
            {
                _hitPoint++;
                CheckDestroy();
            }
            break;
        }
        //Debug.LogFormat("{0} hit {1}", Type.ToString(), interactorType.ToString());
    }

    private void CheckDestroy()
    {
        if(_hitPoint >= _maxPoint)
        {
            DeInit();
        }

        UpdateVisual();
    }

    private void UpdateVisual()
    {
        Color newColor = _renderer.color;
        newColor.a = (_maxPoint - _hitPoint)/_maxPoint;
        _renderer.color = newColor;
    }
    #endregion
}