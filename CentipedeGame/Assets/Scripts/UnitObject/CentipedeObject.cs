﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CentipedeObject : UnitObject
{
    #region Inpector Properties
    [SerializeField] private Sprite _Image_Body;
    [SerializeField] private Sprite _Image_Head;
    #endregion

    #region Public Properties
    public override UnitObjectTypes Type => UnitObjectTypes.Centipede;
    #endregion

    #region Private Properties
    private bool _isFirst = false;
    private CentipedeObject _parentCentipede = null;
    private DirectionTypes _currentHorizontalDirectionType = DirectionTypes.East;
    private DirectionTypes _currentVerticalDirectionType = DirectionTypes.South;
    private SpriteRenderer _renderer;
    #endregion

    #region Events
    public event UnityAction<Vector2> OnMoved;
    public event UnityAction OnUpdateDirectionType;
    public event UnityAction OnDeInit;
    public event UnityAction<CentipedeObject> OnSwitchParent;
    #endregion

    #region Methods
    public void Init(CentipedeObject parent = null)
    {
        gameObject.SetActive(true);
        _parentCentipede = parent;
        Vector2 currentPosition = new Vector2(transform.position.x, transform.position.y);
        DoUpdatePosition(currentPosition, currentPosition);

        _isFirst = _parentCentipede == null;
        _renderer = gameObject.GetComponent<SpriteRenderer>();
        if (!_isFirst)
        {
            InitEvents();
            _renderer.sprite = _Image_Body;
        }
        else
        {
            _renderer.sprite = _Image_Head;
        }
    }

    public void ReInit(Vector2 startPosition, CentipedeObject parent = null)
    {
        DeInit();

        gameObject.SetActive(true);
        DeInitEvents();
        _parentCentipede = parent;
        DoUpdatePosition(startPosition, startPosition);

        _currentHorizontalDirectionType = DirectionTypes.East;
        _currentVerticalDirectionType = DirectionTypes.South;

        _isFirst = _parentCentipede == null;
        _renderer = gameObject.GetComponent<SpriteRenderer>();
        if (!_isFirst)
        {
            InitEvents();
            _renderer.sprite = _Image_Body;
        }
        else
        {
            _renderer.sprite = _Image_Head;
        }
    }

    private void InitEvents()
    {
        if (_parentCentipede == null)
        {
            return;
        }

        _parentCentipede.OnMoved += OnParentMovedAction;
        _parentCentipede.OnUpdateDirectionType += OnParentUpdateDirectionType;
        _parentCentipede.OnDeInit += OnParentDeInitAction;
        _parentCentipede.OnSwitchParent += OnSwitchParentAction;
    }

    private void DeInitEvents()
    {
        if(_parentCentipede == null)
        {
            return;
        }

        _parentCentipede.OnMoved -= OnParentMovedAction;
        _parentCentipede.OnUpdateDirectionType -= OnParentUpdateDirectionType;
        _parentCentipede.OnDeInit -= OnParentDeInitAction;
        _parentCentipede.OnSwitchParent -= OnSwitchParentAction;
    }

    private void OnParentUpdateDirectionType()
    {
        _currentHorizontalDirectionType = _parentCentipede._currentHorizontalDirectionType;
        _currentVerticalDirectionType = _parentCentipede._currentVerticalDirectionType;
    }

    private void OnParentMovedAction(Vector2 position)
    {
        UpdatePosition(position);
    }

    private void OnParentDeInitAction()
    {
        DeInitEvents();
        if (OnSwitchParent != null)
        {
            OnSwitchParent.Invoke(this);
        }
        else
        {
            DoSwitchParent(null);
        }
    }

    private void OnSwitchParentAction(CentipedeObject centipede)
    {
        DeInitEvents();
        if (OnSwitchParent != null)
        {
            OnSwitchParent.Invoke(this);
        }
        else
        {
            DoSwitchParent(null);
        }
        centipede.DoSwitchParent(this);
    }

    public void DoSwitchParent(CentipedeObject centipede)
    {
        Init(centipede);
        if(_isFirst)
        {
            _currentHorizontalDirectionType = GetOppositeDirection(_currentHorizontalDirectionType);
            GameManager.Instance.RequestStartMoveCentipede(this);
        }
    }

    public void StartMove(float speed)
    {
        _timeSpeedInterval = speed;
        StartCoroutine(IEDoMove());
    }

    protected override void CalculateRelation(UnitObjectTypes interactorType)
    {
        switch (interactorType)
        {
            case UnitObjectTypes.Bullet:
            {
                DeInit();
                GameManager.Instance.RequestOnUnitObjectDestroyed(this);
            }
            break;
        }
        //Debug.LogFormat("{0} hit {1}", Type.ToString(), interactorType.ToString());
    }

    protected override void UpdatePosition(Vector2 updatedPosition)
    {
        OnUpdateDirectionType?.Invoke();
        base.UpdatePosition(updatedPosition);
        OnMoved?.Invoke(PreviousPosition);
    }

    private DirectionTypes GetOppositeDirection(DirectionTypes directionType)
    {
        switch (directionType)
        {
            case DirectionTypes.North: return DirectionTypes.South;
            case DirectionTypes.East: return DirectionTypes.West;
            case DirectionTypes.South: return DirectionTypes.North;
            case DirectionTypes.West: return DirectionTypes.East;
        }

        return directionType;
    }

    private IEnumerator IEDoMove()
    {
        while (true)
        {
            yield return new WaitForSeconds(_timeSpeedInterval);
            DoMove();
        }
    }

    private void DoMove()
    {
        Dictionary<DirectionTypes, TileData> tileAdjacentGrid = BoardManager.Instance.GetAdjacentTileData(CurrentPosition);
        Dictionary<DirectionTypes, TileData> tileAdjacentGridTmp = new Dictionary<DirectionTypes, TileData>(tileAdjacentGrid);

        // filter out invalid tile
        foreach (KeyValuePair<DirectionTypes, TileData> tile in tileAdjacentGridTmp)
        {
            if(IsTileHasMushroom(tile.Value) == false)
            {
                tileAdjacentGrid[tile.Key] = null;
            }
        }

        // if adjacent tile in horizontal direction does exist
        if(tileAdjacentGrid[_currentHorizontalDirectionType] != null)
        {
            UpdatePosition(tileAdjacentGrid[_currentHorizontalDirectionType].TilePosition);
            UpdateFaceDirection(_currentHorizontalDirectionType);
        }
        // if adjacent tile in horizontal direction doesn't exist
        else
        {
            _currentHorizontalDirectionType = GetOppositeDirection(_currentHorizontalDirectionType);

            // if adjacent tile in vertical direction doesn't exist
            if (tileAdjacentGrid[_currentVerticalDirectionType] == null)
            {
                _currentVerticalDirectionType = GetOppositeDirection(_currentVerticalDirectionType);
            }

            UpdatePosition(tileAdjacentGrid[_currentVerticalDirectionType].TilePosition);
            UpdateFaceDirection(_currentVerticalDirectionType);
        }
    }

    private void UpdateFaceDirection(DirectionTypes directionType)
    {
        float rotateAngle = 0.0f;
        switch (directionType)
        {
            case DirectionTypes.North:
            {
                rotateAngle = 90.0f;
            }
            break;

            case DirectionTypes.East:
            {
                rotateAngle = 0.0f;
            }
            break;

            case DirectionTypes.South:
            {
                rotateAngle = -90.0f;
            }
            break;

            case DirectionTypes.West:
            {
                rotateAngle = 180.0f;
            }
            break;
        }

        transform.rotation = Quaternion.Euler(new Vector3(0, 0, rotateAngle));
    }

    public override void DeInit()
    {
        base.DeInit();
        DeInitEvents();
        OnDeInit?.Invoke();
    }
    #endregion
}