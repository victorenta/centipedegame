﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : UnitObject
{
    #region Public Properties
    public override UnitObjectTypes Type => UnitObjectTypes.Player;
    #endregion

    #region Private Methods
    private KeyCode _currentMovePressDownKey = KeyCode.None;
    private List<KeyCode> _moveKeyCode = new List<KeyCode>() { KeyCode.UpArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.LeftArrow };
    private List<KeyCode> _shootKeyCode = new List<KeyCode>() { KeyCode.Space };

    private bool _isMoveEnabled = false;
    private Coroutine _moveCoroutine;
    private int _xDiff = 0;
    private int _yDiff = 0;

    private List<TileData> _validTileDataList = new List<TileData>();
    private TileData _nextTile;
    #endregion

    #region Methods
    public void Init(Vector2 startPosition, float speed)
    {
        gameObject.SetActive(true);
        UIManager.Instance.OnKeyPressDown += CheckKeyPressDown;
        UIManager.Instance.OnKeyPressUp += CheckKeyPressUp;

        _timeSpeedInterval = speed;

        Vector2 currentPosition = startPosition;
        DoUpdatePosition(currentPosition, currentPosition);
        _validTileDataList = BoardManager.Instance.GetValidTileForPlayerObject();
    }

    public void ReInit(Vector2 startPosition, float speed)
    {
        gameObject.SetActive(true);
        StopAllCoroutines();
        _timeSpeedInterval = speed;

        Vector2 currentPosition = startPosition;
        DoUpdatePosition(currentPosition, currentPosition);
    }

    private void CheckKeyPressDown(KeyCode inputKey)
    {
        if(_moveKeyCode.Contains(inputKey))
        {
            _currentMovePressDownKey = inputKey;
            if (_isMoveEnabled == false)
            {
                _moveCoroutine = StartCoroutine(IEDoMove());
                _isMoveEnabled = true;
            }
        }

        if (_shootKeyCode.Contains(inputKey))
        {
            DoShootBullet();
        }
    }

    private void CheckKeyPressUp(KeyCode inputKey)
    {
        if(_currentMovePressDownKey == inputKey)
        {
            _currentMovePressDownKey = KeyCode.None;
            if (_isMoveEnabled == true)
            {
                StopCoroutine(_moveCoroutine);
                _isMoveEnabled = false;
            }
        }
    }

    private IEnumerator IEDoMove()
    {
        while (true)
        {
            DoMove();
            yield return new WaitForSeconds(_timeSpeedInterval);
        }
    }

    private void DoMove()
    {
        switch (_currentMovePressDownKey)
        {
            case KeyCode.UpArrow:
            {
                _xDiff = 0;
                _yDiff = 1;
            }
            break;

            case KeyCode.RightArrow:
            {
                _xDiff = 1;
                _yDiff = 0;
            }
            break;

            case KeyCode.DownArrow:
            {
                _xDiff = 0;
                _yDiff = -1;
            }
            break;

            case KeyCode.LeftArrow:
            {
                _xDiff = -1;
                _yDiff = 0;
            }
            break;
        }

        _nextTile = _validTileDataList.Find(x => x.TilePosition == new Vector2(transform.position.x + _xDiff, transform.position.y + _yDiff));
        if(_nextTile != null)
        {
            if(IsTileHasMushroom(_nextTile))
            {
                UpdatePosition(_nextTile.TilePosition);
            }
        }
    }

    private void DoShootBullet()
    {
        GameManager.Instance.RequestShootBullet();
    }

    protected override void CalculateRelation(UnitObjectTypes interactorType)
    {
        switch (interactorType)
        {
            case UnitObjectTypes.Centipede:
            {
                DeInit();
                GameManager.Instance.RequestOnUnitObjectDestroyed(this);
            }
            break;
        }
        //Debug.LogFormat("{0} hit {1}", Type.ToString(), interactorType.ToString());
    }
    #endregion
}