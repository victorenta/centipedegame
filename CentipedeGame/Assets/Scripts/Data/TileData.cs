﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TileData
{
    #region Public Properties
    public Vector2 TilePosition { get { return _tilePosition; } }
    #endregion

    #region Private Properties
    private Vector2 _tilePosition = new Vector2();
    private List<UnitObjectTypes> _occupantTypeList = new List<UnitObjectTypes>();
    #endregion

    #region Events
    public event UnityAction<UnitObjectTypes> OnBeInteracted;
    #endregion

    #region Contructors
    public TileData(Vector2 position)
    {
        _tilePosition = position;
    }
    #endregion

    #region Methods
    public void SubscribeUnit(UnitObjectTypes unitType)
    {
        _occupantTypeList.Add(unitType);
        if (_occupantTypeList.Count > 1)
        {
            OnBeInteracted?.Invoke(unitType);
        }
    }

    public void UnsubscribeUnit(UnitObjectTypes unitType)
    {
        if(_occupantTypeList.Contains(unitType))
        {
            _occupantTypeList.Remove(unitType);
        }
    }

    public void UnsubscribeAllUnit()
    {
        _occupantTypeList.Clear();
    }

    public bool IsHasUnitType(params UnitObjectTypes[] types)
    {
        foreach(UnitObjectTypes type in types)
        {
            if(_occupantTypeList.Contains(type))
            {
                return true;
            }
        }

        return false;
    }
    #endregion
}